﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_5
{
    class FileLogger : AbstractLogger
    {
        private string filePath;

        public FileLogger(MessageType messageType, string filePath) : base(messageType)
        {
            this.filePath = filePath;
        }

        protected override void WriteMessage(string message, MessageType type)
        {
            string whatWillBeWritten = "";
            if ((type & MessageType.ERROR) != 0)
            {
                whatWillBeWritten += "ERROR-";
            }
            if ((type & MessageType.INFO) != 0)
            {
                whatWillBeWritten += "INFO-";
            }
            if ((type & MessageType.WARNING) != 0)
            {
                whatWillBeWritten += "WARNING-";
            }
            whatWillBeWritten += message;
            var file = System.IO.File.AppendText(filePath);
            file.WriteLine(DateTime.Now.ToLongTimeString() + ":" + whatWillBeWritten);
            file.Close();
        }
    }
}
