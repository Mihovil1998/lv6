﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "C:\\Users\\Mihovil\\Desktop\\4. semestar\\RPPOON\\LV6\\Z5.txt");
            logger.Log("404", MessageType.ERROR | MessageType.WARNING);
            fileLogger.Log("404", MessageType.ERROR | MessageType.WARNING);
            Console.WriteLine(System.IO.File.ReadAllText("C:\\Users\\Mihovil\\Desktop\\4. semestar\\RPPOON\\LV6\\Z5.txt"));
        }
    }
}