﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class Program
    {
        static void Main(string[] args)
        {
            PasswordValidator validator = new PasswordValidator(new StringDigitChecker());
            validator.AddStringChecker(new StringLengthChecker(8));
            validator.AddStringChecker(new StringUpperCaseChecker());
            validator.AddStringChecker(new StringLowerCaseChecker());
            string[] passwords = { "lozinka", "l0zinkaa", "L0Z1Nk44" };
            for (int i = 0; i < passwords.Length; i++)
            {
                Console.Write("\"" + passwords[i] + "\" ");
                if (validator.CheckPassword(passwords[i]))
                    Console.Write("is");
                else
                    Console.Write("is not");
                Console.WriteLine(" a valid password.");
            }
        }
    }
}