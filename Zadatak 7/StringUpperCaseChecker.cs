﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class StringUpperCaseChecker : StringChecker
    {
        public StringUpperCaseChecker()
        {
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            for (int i = 0; i < stringToCheck.Length; i++)
                if (stringToCheck[i] >= 'A' && stringToCheck[i] <= 'Z')
                    return true;
            return false;
        }
    }
}
