﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class StringDigitChecker : StringChecker
    {
        public StringDigitChecker()
        {
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            for (int i = 0; i < stringToCheck.Length; i++)
                if (stringToCheck[i] <= '9' && stringToCheck[i] >= '0')
                    return true;
            return false;
        }
    }
}
