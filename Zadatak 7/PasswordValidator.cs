﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_7
{
    class PasswordValidator
    {
        private List<StringChecker> list;

        public PasswordValidator(StringChecker first)
        {
            list = new List<StringChecker>();
            list.Add(first);
        }

        public void AddStringChecker(StringChecker next)
        {
            list[list.Count - 1].SetNext(next);
            list.Add(next);
        }

        public bool CheckPassword(string password)
        {
            return list[0].Check(password);
        }
    }
}
