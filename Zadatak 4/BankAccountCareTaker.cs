﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_4
{
    class BankAccountCareTaker
    {
        private List<BankAccountMemento> list;

        public BankAccountCareTaker() => list = new List<BankAccountMemento>();

        public BankAccountMemento PreviousState
        {
            get
            {
                BankAccountMemento last = list[list.Count - 1];
                list.RemoveAt(list.Count - 1);
                return last;
            }
            set
            {
                list.Add(value);
            }
        }
    }
}
