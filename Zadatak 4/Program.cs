﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Mihovil Kovacevic", "Matije Gupca 67, Zupanja, Croatia", 0);
            var bankAccountCareTaker = new BankAccountCareTaker();
            bankAccountCareTaker.PreviousState = bankAccount.StoreState();
            bankAccount.UpdateBalance(1000);
            bankAccountCareTaker.PreviousState = bankAccount.StoreState();
            bankAccount.UpdateBalance(50000);
            bankAccountCareTaker.PreviousState = bankAccount.StoreState();
            for (int i = 0; i < 3; i++)
            {
                var memento = bankAccountCareTaker.PreviousState;
                Console.WriteLine(memento.OwnerName);
                Console.WriteLine(memento.OwnerAddress);
                Console.WriteLine(memento.Balance);
                Console.WriteLine('\n');
            }
        }
    }
}