﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_4
{
    class BankAccountMemento
    {
        public string OwnerName { get; private set; }

        public string OwnerAddress { get; private set; }

        public decimal Balance { get; private set; }

        public BankAccountMemento(string owner, string address, decimal balance)
        {
            OwnerName = owner;
            OwnerAddress = address;
            Balance = balance;
        }
    }
}