﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Title 1", "This is note 1"));
            notebook.AddNote(new Note("Title 2", "This is note 2"));
            IAbstractIterator iterator = notebook.GetIterator();
            while (!iterator.IsDone)
            {
                Note note = (Zadatak_1.Note)iterator.Current;
                note.Show();
                iterator.Next();
            }
        }
    }
}