﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1
{
    class NotebookIterator : IAbstractIterator
    {
        private Notebook notebook;
        private int currentPosition;

        public NotebookIterator(Notebook notebook)
        {
            this.notebook = notebook;
            this.currentPosition = 0;
        }

        public bool IsDone { get { return this.currentPosition >= this.notebook.Count; } }

        public object Current { get { return this.notebook[this.currentPosition]; } }

        public object First() { return this.notebook[0]; }

        public object Next()
        {
            this.currentPosition++;
            if (this.IsDone)
            {
                return null;
            }
            else
            {
                return this.notebook[this.currentPosition];
            }
        }
    }
}