﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    interface IAbstractIterator
    {
        object First();

        object Next();

        bool IsDone { get; }

        object Current { get; }
    }
}