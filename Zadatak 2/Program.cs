﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("calculator", 100));
            box.AddProduct(new Product("smartphone", 1000));
            IAbstractIterator iterator = box.GetIterator();
            while (!iterator.IsDone)
            {
                Product product = (Zadatak_2.Product)iterator.Current;
                Console.WriteLine(product);
                iterator.Next();
            }
        }
    }
}