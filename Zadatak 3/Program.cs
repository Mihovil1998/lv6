﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem toDoItem = new ToDoItem("Test the ToDoItem class", "Test the ToDoItem class", DateTime.Now);
            CareTaker careTaker = new CareTaker
            {
                PreviousState = toDoItem.StoreState()
            };
            toDoItem = new ToDoItem("Test the CareTaker class", "Test the CareTaker class", DateTime.Now);
            careTaker.PreviousState = toDoItem.StoreState();
            toDoItem = new ToDoItem("Test the Memento class", "Test the Memento class", DateTime.Now);
            careTaker.PreviousState = toDoItem.StoreState();
            for (int i = 0; i < 3; i++)
            {
                var memento = careTaker.PreviousState;
                Console.WriteLine(memento.Title + ": " + memento.Text);
                Console.WriteLine(memento.TimeDue);
            }
        }
    }
}