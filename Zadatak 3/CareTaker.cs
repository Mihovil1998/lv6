﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class CareTaker
    {
        private List<Memento> list;

        public CareTaker() => list = new List<Memento>();

        public Memento PreviousState
        {
            get
            {
                Memento last = list[list.Count - 1];
                list.RemoveAt(list.Count - 1);
                return last;
            }
            set
            {
                list.Add(value);
            }
        }
    }
}