﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker digitChecker = new StringDigitChecker();
            StringChecker lengthChecker = new StringLengthChecker(8);
            StringChecker upperCaseChecker = new StringUpperCaseChecker();
            StringChecker lowerCaseChecker = new StringLowerCaseChecker();
            digitChecker.SetNext(lengthChecker);
            lengthChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lowerCaseChecker);
            string[] passwords = { "lozinka", "l0zinkaa", "L0Z1Nk44" };
            for (int i = 0; i < passwords.Length; i++)
            {
                Console.Write("\"" + passwords[i] + "\" ");
                if (digitChecker.Check(passwords[i]))
                    Console.Write("is");
                else
                    Console.Write("is not");
                Console.WriteLine(" a valid password.");
            }
        }
    }
}