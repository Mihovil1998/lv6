﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_6
{
    class StringLowerCaseChecker : StringChecker
    {
        public StringLowerCaseChecker()
        {
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            for (int i = 0; i < stringToCheck.Length; i++)
                if (stringToCheck[i] >= 'a' && stringToCheck[i] <= 'z')
                    return true;
            return false;
        }
    }
}
