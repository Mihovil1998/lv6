﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_6
{
    class StringLengthChecker : StringChecker
    {
        private int minimumLength;

        public StringLengthChecker(int minimumLength)
        {
            if (minimumLength <= 0)
                throw new Exception("Minimum length cannot be zero or negative!");
            this.minimumLength = minimumLength;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length >= minimumLength;
        }
    }
}
